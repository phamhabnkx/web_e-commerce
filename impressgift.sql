-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 07, 2018 lúc 07:16 AM
-- Phiên bản máy phục vụ: 10.1.34-MariaDB
-- Phiên bản PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `impressgift`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `is_feature` int(11) NOT NULL DEFAULT '0',
  `avata` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name_slug`, `description`, `content`, `price`, `category_id`, `is_feature`, `avata`) VALUES
(8, 'iNB106 – Eco Friendly NoteBook', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 13, 1, '/administrator/uploads/layer7.png'),
(9, 'IMP4102-Memo desk set', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 13, 1, '/administrator/uploads/layer1.png'),
(10, 'IMP4102-Memo desk set', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 13, 1, '/administrator/uploads/layer2.png'),
(12, 'IMP4102-Memo desk set', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 17, 1, '/administrator/uploads/layer3.png'),
(13, 'IMP4102-Memo desk set', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.\r\n\r\n', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 13, 1, '/administrator/uploads/layer4.png'),
(15, 'IMP4102-Memo desk set', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 13, 1, '/administrator/uploads/layer6.png'),
(16, 'IMP4102-Memo desk set', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum.', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 13, 1, '/administrator/uploads/layer8.png'),
(17, 'IMP4102-Memo desk set', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 13, 1, '/administrator/uploads/layer5.png'),
(18, 'IMP4102-Memo desk set', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia.', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 5, 0, '/administrator/uploads/iGS508-300x300.jpg'),
(19, 'IMP4102-Memo desk set', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 5, 0, '/administrator/uploads/iGS509-300x300.jpg'),
(20, 'IMP4102-Memo desk set', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.\r\n\r\n', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 5, 0, '/administrator/uploads/iGS508-300x300.jpg'),
(21, 'IMP4102-Memo desk set', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia.', '<p>Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. Aenean sed augue et sem blandit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin massa nibh, ornare a condimentum eu, malesuada sit amet est. Praesent ac ligula at nisl feugiat dignissim. Nulla id porta dolor. Morbi pretium mattis nulla, nec iaculis nibh pretium ut. Ut et quam enim. Morbi tincidunt turpis at elit tristique feugiat.</p>\r\n', 10, 5, 0, '/administrator/uploads/iGS508-300x300.jpg'),
(22, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item1.png'),
(23, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.\r\n\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item2.png'),
(24, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item3.png'),
(25, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item4.png'),
(26, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item5.png'),
(27, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item6.png'),
(28, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item7.png'),
(29, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item8.png'),
(30, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item1.png'),
(31, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 9, 14, 0, '/administrator/uploads/item2.png'),
(32, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item1.png'),
(33, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item1.png'),
(34, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item8.png'),
(35, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item11.png'),
(37, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item7.png'),
(38, 'DW5001B-Rectangle MEMO Bottle', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>\r\n', 10, 14, 0, '/administrator/uploads/item6.png'),
(40, 'IMP4102-Memo desk set', 'zxd', '<p>&aacute;d</p>\r\n', 10, 14, 0, '/administrator/uploads/item11.png'),
(41, 'IMP4102-Memo desk set', 'dfghj', '<p>sadf</p>\r\n', 10, 5, 0, '/administrator/uploads/iGS508-300x300.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `Name_product` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `order_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_categories`
--

INSERT INTO `product_categories` (`id`, `Name_product`, `name_slug`, `description`, `order_by`) VALUES
(5, 'New Arrivals', 'new_arrivals', '  Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', 1),
(6, 'Promotions', 'promotions', ' Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia.  ', 2),
(7, 'Apparel', 'apparel', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia.', 3),
(8, 'Accesspries', 'accesspries', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', 4),
(9, 'Carrier', 'carrier', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', 5),
(10, 'Drinkware', 'drinkware', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', 6),
(11, 'Eco Series', 'eco_series', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia.', 7),
(12, 'Excutive', 'excutive', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', 8),
(13, 'Gift sets', 'Gift_sets', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', 9),
(14, 'Impressive Umberllas', 'impressive_umberllas', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia.', 10),
(15, 'Life Style', 'life_style', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', 11),
(16, 'Office', 'office', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia.', 12),
(17, 'Outdoor', 'outdoor', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia. ', 13),
(18, 'Stationeries', 'stationeries', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia.', 14),
(19, 'Travel', 'travel', 'Mauris nibh nulla, iaculis vel commodo nec, fermentum ut risus. Duis porttitor accumsan felis, id porta enim faucibus lacinia.', 15);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `table_project`
--

CREATE TABLE `table_project` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avata` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `table_project`
--

INSERT INTO `table_project` (`id`, `name`, `avata`) VALUES
(2, 'DW5001B-Rectangle MEMO Bottle', '/administrator/uploads/item5.png'),
(3, 'DW5001B-Rectangle MEMO Bottle', '/administrator/uploads/item1.png'),
(4, 'DW5001B-Rectangle MEMO Bottle', '/administrator/uploads/item2.png'),
(5, 'DW5001B-Rectangle MEMO Bottle', '/administrator/uploads/item3.png'),
(6, 'DW5001B-Rectangle MEMO Bottle', '/administrator/uploads/item4.png'),
(7, 'DW5001B-Rectangle MEMO Bottle', '/administrator/uploads/item6.png'),
(8, 'DW5001B-Rectangle MEMO Bottle', '/administrator/uploads/item7.png'),
(10, 'DW5001B-Rectangle MEMO Bottle', '/administrator/uploads/item11.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `table_user`
--

CREATE TABLE `table_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `table_user`
--

INSERT INTO `table_user` (`id`, `username`, `password`, `role`, `email`, `phone`) VALUES
(2, 'hâm', '202cb962ac59075b964b07152d234b70', 2, ' phamhabnkx@gmail.com ', '  '),
(3, 'gà ', '202cb962ac59075b964b07152d234b70', 3, ' phamhabnkx@gmail.com ', ' 123 '),
(4, 'ha', '123', 1, 'phamhabnkx@gmail.com', NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `table_project`
--
ALTER TABLE `table_project`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `table_user`
--
ALTER TABLE `table_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT cho bảng `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `table_project`
--
ALTER TABLE `table_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `table_user`
--
ALTER TABLE `table_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
