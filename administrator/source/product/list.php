<section class="content-header">
    <h1>
        Danh sách sản phẩm
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Danh sách sản phẩm</li>
    </ol>
</section>
<section class="content">
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Mô tả</th>
                    <th>Nội dung</th>
                    <th>Giá sản phẩm</th>
                    <th>Mục sản phẩm</th>
                    <th>ảnh</th>
                    <th></th>
                    <th></th>

                </tr>
            </thead>
            <tbody>
           	    <?php 
                    $sql= "SELECT *FROM products ORDER BY id DESC";
           	        $result = mysqli_query($con,$sql);
           	        if(mysqli_num_rows($result) >0){
           	        	$i = 1;
           	        	while( $row = mysqli_fetch_assoc($result) ){
           	    ?>
           	                <tr>
           	                	<td><?php echo $i; ?></td>
           	                	<td><?php echo $row['name_slug']; ?></td>
           	                	<td><?php echo $row['description']; ?></td>
           	                	<td>
                                <?php echo $row['content']; ?>  
                              </td>
           	                	<td><?php echo $row['price']; ?></td>
           	                	<td><?php echo $row['category_id']; ?></td>
           	                	<td><img src="<?php echo $row['avata']; ?>" width="80"></td>
                              <td class="text-center">
                                <?php
                                   if($row['is_feature'] == 1){
                                ?>
                                    <i class="fa fa-star"></i>
                                <?php    
                                   }
                                ?>
                              </td>
           	                	<td class="text-center">
           	                		<a href="/administrator?action=product/edit&param=<?php echo $row['id']; ?>" class="btn btn-success" title="Sửa">Sửa</a>
           	                	</td>
           	                	<td class="text-center">
           	                		<a href="/administrator?action=product/delete&param=<?php echo $row['id']; ?>" class="btn btn-danger" title="xóa">Xóa</a>
           	                	</td>
           	                </tr>
           	    <?php
           	                $i++;
           	            }
                    }
           	    ?>            
            </tbody>
        </table>
    <nav class="clearfix text-center">
        <ul class="pagination">
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">>></a></li>
        </ul>
    </nav>
    </div>
</section>
