<?php include('config.php'); ?>

<?php
    if( empty($_GET['param']) ){
        echo '<script type="text/javascript">';
        echo 'alert("Lỗi !");';
        echo 'window.location.href = "/administrator/?action=product/list";';
        echo '</script>';
    }
    $id = $_GET['param'];
    $sql = " SELECT * FROM products WHERE id = $id";
    $result = mysqli_query($con,$sql);
    if (mysqli_num_rows($result) <= 0 ){
        header('location:/administrator?action=product/list');die;    
    }
    $result = mysqli_fetch_assoc($result);
    if(!$result){
        echo '<script type="text/javascript">'; 
        echo 'alert("Dữ liệu không tồn tại!");'; 
        echo 'window.location.href = "/administrator/?action=product/list";';
        echo '</script>';   
    } 

?>

<?php 
if(isset($_POST['submit']) ){
    if( !empty($_POST['title']) && !empty($_POST['category']) && !empty($_FILES['file']['name']) && !empty($_POST['short_description']) && !empty($_POST['price']) && !empty('content') ){
        $title = $_POST['title'];
        $category= $_POST['category'];
        $short_des =$_POST['short_description'];
        $price = $_POST['price'];       
        $content = $_POST['content'];
        $is_feature=0;
        if(!empty($_POST['is_feature']) ){
            $is_feature=1;
        }
        if( $_FILES['file']['type'] == "image/jpeg" || $_FILES['file']['type'] == "image/png" || $_FILES['file']['type'] == "image/gif"){
            // var_dump($_FILES['file']);die;
            $path="uploads/";
            $tmp_name=$_FILES['file']['tmp_name'];
            $name=$_FILES['file']['name'];
            $avatar='/administrator/'.$path.$name;
            move_uploaded_file($tmp_name, $path.$name);
            $sql = "UPDATE products SET name_slug ='$title', description ='$short_des', content ='$content', price='$price', category_id='$category', is_feature='$is_feature', avata='$avatar' WHERE id=$id";
           
            $result=mysqli_query($con, $sql);
            if($result){
                echo "<script>";
                echo "alert('Sửa Sản phẩm thành công');";
                echo "window.location.href ='/administrator?action=product/list';";
                echo "</script>";
            }else{
                echo "<script>";
                echo "alert('Lỗi:".mysqli_error($con)."');";
                echo "</script>";
            }
        }else{
            echo "<script>";
            echo "alert('File upload is a file image');";
            echo "window.location.href ='/administrator?action=product/add';";
            echo "</script>";
        }
        
        
    }
}

?>

<section class="content-header">
    <h1>
        Sửa sản phẩm <?php echo $result['name_slug']; ?>
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sửa <?php echo $result['name_slug']; ?></li>
    </ol>
</section>

<section class="content">
 <form action="" method="POST" enctype="multipart/form-data">
    <div class="row">
    	<div class="col-md-3"></div>
    	<div class="col-md-6">
    		<div class="form-group">
    		    <label>Tiêu đề</label>
    		    <input type="text" value="<?php echo $result['name_slug'];?>" class="form-control" required name="title">
    	    </div>
            <div class="form-group">
                <label>Danh mục sản phẩm</label>
                <select name="category" required class="form-control">
                    <option>---------chọn danh mục sản phẩm--------</option>
                    <?php
                        $sql1= "SELECT *FROM product_categories ORDER BY id DESC";
                        $result1 = mysqli_query($con,$sql1);
                        if(mysqli_num_rows($result1) > 0 ){
                           while($dong=mysqli_fetch_assoc($result1) ){
                           ?>
                        <option <?php if($dong['id']==$result['category_id']){ echo 'selected';} ?> value="<?php echo $dong['id']; ?>"><?php echo $dong['Name_product']; ?></option>

                        <?php         

                        }
                    }    
                        
                    ?>
                </select>
            </div>
            <div class="form-group">
                <img src="<?php echo $result['avata']; ?>" alt="" width="100px">
            </div>
	    	<div class="form-group">
                <label>Ảnh sản phẩm</label>
                <input type="file" name="file" value="<?php echo $result['avata'];?>" class="form-control" required>      
            </div>
            <div class="form_group">
                <label>Mô tả ngắn</label>
                <textarea name="short_description" rows="5" class="form-control"><?php echo $result['description'];?></textarea>
            </div>
            <div class="form-group">              
                <label>Giá sản phẩm</label>
                <input type="number" name="price" value="<?php echo $result['price'];?>" class="form-control">
            </div>
            <div class="form-group">              
                <label>Is_feature</label>
                <input type="checkbox" <?php if($result['is_feature']==1){echo 'checked';} ?> name="is_feature" value="1">
            </div>
            <div class="form-group">
                <label>Nội dung</label>
                <textarea name="content" id="content" rows="6" class="form-control" required></textarea>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary" name="submit">Edit</button>
                <button type="reset" class="btn btn-default" name="reset"><a href="/administrator/?action=product/list" style="text-decoration: none; color: black;">Reset</a></button>   
            </div>
    	</div>
    	<div class="col-md-3"></div> 	
    </div>
 </form>
</section>
<script>
    window.onload = function(){
        window.content = <?php echo json_encode($result['content']);?>;
        CKEDITOR.instances['content'].setData(window.content);
    } 
</script>



