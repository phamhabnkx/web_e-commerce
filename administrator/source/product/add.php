<?php include('config.php'); ?>

<?php 
if(isset($_POST['submit']) ){
	if( !empty($_POST['title']) && !empty($_POST['category']) && !empty($_FILES['file']['name']) && !empty($_POST['short_description']) && !empty($_POST['price']) && !empty('content') ){
		$title = $_POST['title'];
		$category= $_POST['category'];
        $short_des =$_POST['short_description'];
        $price = $_POST['price'];       
        $content = $_POST['content'];
        $is_feature=0;
        if(!empty($_POST['is_feature']) ){
            $is_feature=1;
        }
        if( $_FILES['file']['type'] == "image/jpeg" || $_FILES['file']['type'] == "image/png" || $_FILES['file']['type'] == "image/gif"){
            // var_dump($_FILES['file']);die;
            $path="uploads/";
            $tmp_name=$_FILES['file']['tmp_name'];
            $name=$_FILES['file']['name'];
            $avatar='/administrator/'.$path.$name;
            move_uploaded_file($tmp_name, $path.$name);
            $sql = "INSERT INTO products (name_slug, description, content, price, category_id, is_feature, avata) VALUES ('$title', '$short_des', '$content', '$price', '$category', '$is_feature', '$avatar')";
            $result=mysqli_query($con, $sql);
            if($result){
                echo "<script>";
                echo "alert('Thêm Sản phẩm thành công');";
                echo "window.location.href ='/administrator?action=product/list';";
                echo "</script>";
            }else{
                echo "<script>";
                echo "alert('Lỗi:".mysqli_error($con)."');";
                echo "</script>";
            }
        }else{
            echo "<script>";
            echo "alert('File upload is a file image');";
            echo "window.location.href ='/administrator?action=product/add';";
            echo "</script>";
        }
		
		
	}
}

?>
<section class="content-header">
    <h1>
        Thêm sản phẩm
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Thêm sản phẩm</li>
    </ol>
</section>

<section class="content">
 <form action="" method="POST" enctype="multipart/form-data">
    <div class="row">
    	<div class="col-md-3"></div>
    	<div class="col-md-6">
    		<div class="form-group">
    		    <label>Tiêu đề</label>
    		    <input type="text" class="form-control" required name="title">
    	    </div>
            <div class="form-group">
                <label>Danh mục sản phẩm</label>
                <select name="category" required class="form-control">
                    <option value="">---------chọn danh mục sản phẩm--------</option>
                    <?php
                        $sql= "SELECT *FROM product_categories ORDER BY id DESC";
                        $result = mysqli_query($con,$sql);
                        if(mysqli_num_rows($result) > 0 ){
                        while($dong=mysqli_fetch_assoc($result) ){
                        ?>
                          <option value="<?php echo $dong['id']; ?>"><?php echo $dong['Name_product']; ?></option>
                        <?php 
                        }
                    }

                    ?>
                </select>
            </div>
	    	<div class="form-group">
                <label>Ảnh sản phẩm</label>
                <input type="file" name="file" class="form-control" required>      
            </div>
            <div class="form_group">
                <label>Mô tả ngắn</label>
                <textarea name="short_description" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group">              
                <label>Giá sản phẩm</label>
                <input type="number" name="price" required class="form-control">                               
            </div>
            <div class="form-group">
                    <label>Is_feature</label>
                    <input type="checkbox" name="is_feature" value="1">
            </div>
            <div class="form-group">
                <label>Nội dung</label>
                <textarea name="content" id="content" rows="6" class="form-control" required></textarea>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary" name="submit">Thêm</button>
                <button type="reset" class="btn btn-default" name="reset"><a href="/administrator/?action=product/list" style="text-decoration: none; color: black;">Reset</a></button>   
            </div>
    	</div>
    	<div class="col-md-3"></div> 	
    </div>
 </form>
</section>



