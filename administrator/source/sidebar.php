<ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li>
                            <a href="/administrator">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            <!-- <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                            </span> -->
                            </a>
                            <!-- <ul class="treeview-menu">
                                <li class="active"><a href="$redirec"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                                <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                            </ul> -->
                        </li>
                        <li class="treeview">
                            <a href="#">
                            <i class="fa fa-files-o"></i>
                            <span>Danh mục sản phẩm</span>
                            <span class="pull-right-container">
                            <span class="label label-primary pull-right">2</span>
                            </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/administrator?action=product_categories/add"><i class="fa fa-circle-o"></i> Thêm danh mục sản phẩm </a></li>
                                <li><a href="/administrator?action=product_categories/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                            <i class="fa fa-files-o"></i>
                            <span>Sản phẩm</span>
                            <span class="pull-right-container">
                            <span class="label label-primary pull-right">2</span>
                            </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/administrator?action=product/add"><i class="fa fa-circle-o"></i> Thêm sản phẩm </a></li>
                                <li><a href="/administrator?action=product/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                            <i class="fa fa-files-o"></i>
                            <span>User_admin</span>
                            <span class="pull-right-container">
                            <span class="label label-primary pull-right">2</span>
                            </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/administrator?action=user_admin/add"><i class="fa fa-circle-o"></i> Thêm thành viên </a></li>
                                <li><a href="/administrator?action=user_admin/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                            <i class="fa fa-files-o"></i>
                            <span>Projects</span>
                            <span class="pull-right-container">
                            <span class="label label-primary pull-right">2</span>
                            </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/administrator?action=edit_delete_project/add"><i class="fa fa-circle-o"></i> Thêm dự án </a></li>
                                <li><a href="/administrator?action=edit_delete_project/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
                            </ul>
                        </li>
                       
                    </ul>