<!-- Chèn file config -->
<?php include('config.php'); ?>
<?php
    if( empty($_GET['param']) ){
        echo '<script type="text/javascript">';
        echo 'alert("Lỗi !");';
        echo 'window.location.href = "/administrator/?action=user_admin/list";';
        echo '</script>';
    }
    $id = $_GET['param'];
    $sql = " SELECT * FROM table_user WHERE id =$id";
    $result = mysqli_query($con,$sql);
    if (mysqli_num_rows($result) <= 0 ){
        header('location:/administrator?action=user_admin/list');die;
        
    }
    $result = mysqli_fetch_assoc($result);
    if(!$result){
        echo '<script type="text/javascript">'; 
        echo 'alert("Dữ liệu không tồn tại!");'; 
        echo 'window.location.href = "/administrator/?action=user_admin/list";';
        echo '</script>';   
    } 

?>

<?php  
    // Kiểm tra xem người dùng đã ấn nút submit chưa
    if( isset($_POST['submit']) ) {
        // Kiểm tra xem người dùng đã điền đầy đủ các thông tin chưa
        if( !empty($_POST['user']) && !empty($_POST['password']) && !empty($_POST['re_password']) && !empty($_POST['role']) && !empty($_POST['email'] ) ) {
            $user = $_POST['user'];
            $password =md5($_POST['password']); 
            $re_password =md5($_POST['re_password']);
            $role = $_POST['role'];
            $email = $_POST['email'];
            $phone= $_POST['phone'];
            // Kiểm tra xem password nhập lại có đúng không!
            if( $password != $re_password ) {
                //Tạo thông báo bằng javascript
                echo '<script>alert("Mật khẩu nhập lại không chính xác!")</script>';
                echo 'window.location.href = "/administrator/?action=user_admin/edit";';
            }

            $sql = "UPDATE table_user SET username ='$user', password='$password', role='$role', email ='$email', phone ='$phone' WHERE id=$id ";
            if (mysqli_query($con, $sql)) {
                //Tạo thông báo bằng javascript
                echo '<script type="text/javascript">'; 
                echo 'alert("Sửa thành viên thành công");'; 
                echo 'window.location.href = "/administrator?action=user_admin/list";';
                echo '</script>';
            } else {
                //Tạo thông báo bằng javascript
                echo '<script>alert("sửa thành viên thất bại!")</script>';
            }
        }else {
            //Tạo thông báo bằng javascript
            echo '<script>alert("Vui lòng nhập đầy đủ thông tin!")</script>';
        }
    }

?>
<section class="content-header">
                    <h1>
                        Sửa thông tin thành viên <?php echo $result['username']; ?>
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Sửa thông tin thành viên  <?php echo $result['username']; ?></li>
                    </ol>
</section>

<section class="content">
    <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1 class="text-center">Sửa thông tin thành viên</h1>
            
            <form action="" method="POST">
                <div class="form-group">
                    <label for="">Username: </label>
                    <input type="text" name="user" value="<?php echo $result['username'];?>" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="">Password: </label>
                    <input type="password" name="password" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="">Re-password: </label>
                    <input type="password" name="re_password" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="">Role: </label>
                    <input type="text" value="<?php echo $result['role'];?>" name="role" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="">Email: </label>
                    <input type="text" value="<?php echo $result['email'];?>" name="email" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="">Phone: </label>
                    <input type="text" value="<?php echo $result['phone'];?>" name="phone" class="form-control">
                </div>

                <div class="form-group text-right">
                    <button type="submit" name="submit" class="btn btn-primary">Edit</button>
                    <button type="reset" class="btn btn-default" name="reset"><a href="/administrator?action=user_admin/add" style="text-decoration: none; color: black;">Reset</a></button>
                </div>
            </form>
        </div>
        <div class="col-md-3"></div>
 
</section>