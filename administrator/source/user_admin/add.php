<!-- Chèn file config -->
<?php include('config.php'); ?>

<?php  
	// Kiểm tra xem người dùng đã ấn nút submit chưa
	if( isset($_POST['submit']) ) {
		// Kiểm tra xem người dùng đã điền đầy đủ các thông tin chưa
		if( !empty($_POST['user']) && !empty($_POST['password']) && !empty($_POST['re_password']) && !empty($_POST['role']) && !empty($_POST['email'] ) ) {
			$user = $_POST['user'];
			$password =md5($_POST['password']); 
			$re_password =md5($_POST['re_password']);
			$role = $_POST['role'];
			$email = $_POST['email'];
			$phone= $_POST['phone'];
			// Kiểm tra xem password nhập lại có đúng không!
			if( $password != $re_password ) {
				//Tạo thông báo bằng javascript
				echo '<script>alert("Mật khẩu nhập lại không chính xác!")</script>';
			}

			$sql = "INSERT INTO table_user (username, password, role, email, phone) VALUES ('". $user ."', '". $password ."', '". $role ."', ' ".$email." ', ' ".$phone." ')";
			if (mysqli_query($con, $sql)) {
				//Tạo thông báo bằng javascript
			    echo '<script type="text/javascript">'; 
				echo 'alert("Thêm thành viên thành công");'; 
				echo 'window.location.href = "/administrator?action=user_admin/list";';
				echo '</script>';
			} else {
				//Tạo thông báo bằng javascript
			    echo '<script>alert("Thêm thành viên thất bại!")</script>';
			}
		}else {
			//Tạo thông báo bằng javascript
			echo '<script>alert("Vui lòng nhập đầy đủ thông tin!")</script>';
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Thêm</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<h1 class="text-center">Thêm thành viên</h1>
			
			<form action="" method="POST">
				<div class="form-group">
					<label for="">Username: </label>
					<input type="text" name="user" class="form-control" required>
				</div>

				<div class="form-group">
					<label for="">Password: </label>
					<input type="password" name="password" class="form-control" required>
				</div>

				<div class="form-group">
					<label for="">Re-password: </label>
					<input type="password" name="re_password" class="form-control" required>
				</div>

				<div class="form-group">
					<label for="">Role: </label>
					<input type="text" name="role" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="">Email: </label>
					<input type="text" name="email" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="">Phone: </label>
					<input type="text" name="phone" class="form-control">
				</div>

				<div class="form-group text-right">
					<button type="submit" name="submit" class="btn btn-primary">Submit</button>
					<button type="reset" class="btn btn-default" name="reset"><a href="/administrator?action=user_admin/add" style="text-decoration: none; color: black;">Reset</a></button>
				</div>
			</form>
		</div>
		<div class="col-md-3"></div>
	</div>
</body>
</html>