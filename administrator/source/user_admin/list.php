<section class="content-header">
    <h1>
        Danh sách thành viên
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Danh sách thành viên</li>
    </ol>
</section>
<section class="content">
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Role</th>
                    <th>Email</th>
                    <th>Phonenumber</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
           	    <?php 
           	        $sql= "SELECT *FROM table_user ORDER BY id DESC";
           	        $result = mysqli_query($con,$sql);
           	        if(mysqli_num_rows($result) > 0 ){
           	        	$i = 1;
           	        	while( $row = mysqli_fetch_assoc($result) ){
           	    ?>
           	                <tr>
           	                	<td><?php echo $i; ?></td>
           	                	<td><?php echo $row['username']; ?></td>
           	                	<td><?php echo $row['password']; ?></td>
                              <td><?php echo $row['role']; ?></td>
                              <td><?php echo $row['email']; ?></td>
           	                	<td><?php echo $row['phone']; ?></td>
           	                	<td class="text-center">
           	                		<a href="/administrator?action=user_admin/edit&param=<?php  echo $row['id']; ?>" class="btn btn-success" title="Sửa">Sửa</a>
           	                	</td>
           	                	<td class="text-center">
           	                		<a href="/administrator?action=user_admin/delete&param=<?php  echo $row['id']; ?>" class="btn btn-danger" title="xóa">Xóa</a>
           	                	</td>
           	                </tr>
           	    <?php
           	                $i++;
           	            }
                    }
           	    ?>            
            </tbody>
        </table>
    </div>
</section>