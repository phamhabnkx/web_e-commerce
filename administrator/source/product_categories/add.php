<?php 
if(isset($_POST['submit']) ){
	if( !empty($_POST['title']) && !empty($_POST['slug']) && !empty($_POST['description']) && !empty($_POST['order_by']) ){
		$title = $_POST['title'];
		$slug = $_POST['slug'];
        $orderby = $_POST['order_by'];
		$des =$_POST['description'];
		$sql = "INSERT INTO product_categories (Name_product, name_slug, description, order_by) VALUES ('$title', '$slug', '$des', '$orderby')";
		if(mysqli_query($con, $sql) ){
			echo "<script>";
            echo "alert('Thêm danh muc thành công');";
            echo "window.location.href ='/administrator?action=product_categories/list';";
            echo "</script>";
		}else{
			echo "<script>";
            echo "alert('Lỗi:".mysqli_error($con)."');";
            echo "</script>";
		}
	}
}

?>

<section class="content-header">
    <h1>
        Thêm danh mục sản phẩm
        <small>Control panel</small>
    </h1> 
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Thêm danh mục sản phẩm</li>
    </ol>
</section>

<section class="content">
 <form action="" method="post">
    <div class="row">
    	<div class="col-md-3"></div>
    	<div class="col-md-6">
    		<div class="form-group">
    		    <label>Tiêu đề</label>
    		    <input type="text" class="form-control" required name="title">
    	    </div>
	    	<div class="form-group">
	    		<label>Tên Không dấu</label>
	    	    <input type="text" class="form-control" required name="slug">		
	    	</div>
            <div class="form-group">
                    <label>Order_by</label>
                    <input type="text" name="order_by" required class="form-control">
            </div>
	    	<div class="form-group">
	    		<label>Mô tả</label>
	    		<textarea name="description" class="form-control" required rows="5"></textarea>
	    		
	    	</div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary" name="submit">Thêm</button>
                <button type="reset" class="btn btn-default" name="reset"><a href="/administrator?action=product_categories/add" style="text-decoration: none; color: black;">Reset</a></button>   
            </div>
    	</div>
    	<div class="col-md-3"></div> 	
    </div>
 </form>

</section>



