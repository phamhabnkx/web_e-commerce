<?php include('config.php'); ?>
<?php 
if(isset($_POST['submit']) ){
	if( !empty($_POST['title']) && !empty($_FILES['file']['name']) ){
		$title = $_POST['title'];
		if( $_FILES['file']['type'] == "image/jpeg" || $_FILES['file']['type'] == "image/png" || $_FILES['file']['type'] == "image/gif"){
            // var_dump($_FILES['file']);die;
            $path="uploads/";
            $tmp_name=$_FILES['file']['tmp_name'];
            $name=$_FILES['file']['name'];
            $avatar='/administrator/'.$path.$name;
            move_uploaded_file($tmp_name, $path.$name);
            $sql = "INSERT INTO table_project (name,avata) VALUES ('$title','$avatar')";
            $result=mysqli_query($con, $sql);
            if($result){
                echo "<script>";
                echo "alert('Thêm Sản phẩm thành công');";
                echo "window.location.href ='/administrator?action=edit_delete_project/list';";
                echo "</script>";
            }else{
                echo "<script>";
                echo "alert('Lỗi:".mysqli_error($con)."');";
                echo "</script>";
            }
        }else{
            echo "<script>";
            echo "alert('File upload is a file image');";
            echo "window.location.href ='/administrator?action=edit_delete_project/add';";
            echo "</script>";
        }
		
	}
}

?>

<section class="content-header">
    <h1>
        Thêm dự án
        <small>Control panel</small>
    </h1> 
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Thêm dự án</li>
    </ol>
</section>

<section class="content">
 <form action="" method="post" enctype="multipart/form-data">
    <div class="row">
    	<div class="col-md-3"></div>
    	<div class="col-md-6">
    		<div class="form-group">
    		    <label>Tiêu dự án</label>
    		    <input type="text" class="form-control" required name="title">
    	    </div>
	    	<div class="form-group">
	    		<label>Avata</label>
	    		<input type="file" name="file" class="form-control" required>
	    		
	    	</div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-primary" name="submit">Thêm</button>
                <button type="reset" class="btn btn-default" name="reset"><a href="/administrator?action=edit_delete_project/add" style="text-decoration: none; color: black;">Reset</a></button>   
            </div>
    	</div>
    	<div class="col-md-3"></div> 	
    </div>
 </form>

</section>



