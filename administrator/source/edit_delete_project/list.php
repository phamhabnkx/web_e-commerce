<section class="content-header">
    <h1>
        Danh sách project
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Danh sách dự án</li>
    </ol>
</section>
<section class="content">
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Name_project</th>
                    <th>Avata</th> 
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
           	    <?php 
           	        $sql= "SELECT *FROM table_project ORDER BY id DESC";
           	        $result = mysqli_query($con,$sql);
           	        if(mysqli_num_rows($result) > 0 ){
           	        	$i = 1;
           	        	while( $row = mysqli_fetch_assoc($result) ){
           	    ?>
           	                <tr>
           	                	<td><?php echo $i; ?></td>
           	                	<td><?php echo $row['name']; ?></td>
                              <td><img src="<?php echo $row['avata']; ?>" width="80"></td>
           	                	<td class="text-center">
           	                		<a href="/administrator?action=edit_delete_project/edit&param=<?php  echo $row['id']; ?>" class="btn btn-success" title="Sửa">Sửa</a>
           	                	</td>
           	                	<td class="text-center">
           	                		<a href="/administrator?action=edit_delete_project/delete&param=<?php  echo $row['id']; ?>" class="btn btn-danger" title="xóa">Xóa</a>
           	                	</td>
           	                </tr>
           	    <?php
           	                $i++;
           	            }
                    }
           	    ?>            
            </tbody>
        </table>
    </div>
</section>