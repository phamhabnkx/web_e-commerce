        <header id="header" class="header animated bounceInRight delay-5s"">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-2 col-xs-6">
                        <div class="header-item">
                            <a href="javascript:void(0);" title=""><img class="header-item1" src="images/0-Homepage2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-10">
                        <div class="all">
                            <div class="ship hidden-xs">
                                <div class="item">
                                    <a href="mini.php" title=""><img class="header-item2" src="images/oto.png" alt=""></a>
                                </div>
                                <div class="free">
                                    <h4> free shipping</h4>
                                    <p>Free shipping on all order</p>
                                </div>
                            </div>
                            <div class="ship call-ship hidden-xs">
                                <div class="item">
                                    <img class="header-item3" src="images/call.png" alt="">
                                </div>
                                <div class="free">
                                    <h4> HOTLINE</h4>
                                    <p>+65 6876 0079</p>
                                </div>
                            </div>
                            <div class="ship col-xs-6">
                                <div class="item">
                                    <img class="header-item3" src="images/shop.png" alt="">
                                </div>
                                <div class="free">
                                    <a href="equiry.php" title="">
                                        <h4>ENQUIRY CART </h4>
                                        <p>
                                            (<?php 
                                            if(isset($_SESSION['enquiry'])){
                                                echo count($_SESSION['enquiry']);
                                            }else echo 0;
                                            ?>)
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- /header -->