<div class="menu">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8 col-xs-5">
                <div class="menu-1">
                    <a href="javascript:void(0)" class="menu-bars"><i class="fas fa-bars"></i></a>
                    <ul class="menu-2 menu3">
                        <li class="right-delete">
                            <button class="trigger-verlay mobile-trigger-overlay hidden-md hidden-lg">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </li>
                        <li class="active"><a href="index.php" title="">Home</a></li>
                        <li class="about-us"><a href="About_us.php" title="">About us</a></li>
                        <li><a href="our_products.php" title="">projects</a></li>
                        <li><a href="project2.php" title="">Our products   <i class="fas fa-chevron-down hidden-xs hidden-sm" aria-heddin="true"></i></a><i class="fas fa-chevron-down hidden-md hidden-lg" aria-heddin="true"></i>
                            <ul class="sub-menu">
                                <li id="menu-item-1"><a href="" title="">Accessories</a></li>
                                <li id="menu-item-2"><a href="" title="">Drinkware</a></li>
                                <li id="menu-item-3"><a href="" title="">Stationeries</a></li>
                                <li id="menu-item-4"><a href="" title="">Promotions</a></li>
                                <li id="menu-item-5"><a href="" title="">Life Style</a></li>
                                <li id="menu-item-6"><a href="" title="">Carrier</a></li>
                                <li id="menu-item-8"><a href="" title="">Office</a></li>
                                <li id="menu-item-9"><a href="" title="">Outdoor</a></li>
                                <li id="menu-item-10"><a href="" title="">Gift Sets</a></li>
                                <li id="menu-item-11"><a href="" title="">Impressive Umbrellas</a></li>
                                <li id="menu-item-11"><a href="" title="">Impressive Umbrellas</a></li>
                                <li id="menu-item-11"><a href="" title="">Impressive Umbrellas</a></li>
                            </ul>
                        </li>
                        <li><a href="contacts.php" title="">contact us</a></li>
                        <div class="clearfix"></div>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-7">
                <div class="search">
                    <form>
                        <input class="inpt" type="text" placeholder="search" name="search">
                        <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>