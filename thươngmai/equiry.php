<?php 
    include('config1.php');
    session_start(); 
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>E-commerce project</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="asset/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/asset.css">
    
</head>

<body>
    <?php include('header.php'); ?>
    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-5">
                    <div class="menu-1">
                       <a href="javascript:void(0);" class="menu-bars"><i class="fas fa-bars"></i></a>
                        <ul class="menu-2 menu3">
                             <li class="right-delete">
                                <button class="trigger-verlay mobile-trigger-overlay hidden-md hidden-lg">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </li>
                            <li><a href="index.php" title="">Home</a></li>
                            <li><a href="About_us.php" title="">About us</a></li>
                            <li><a href="our_products.php" title="">projects</a></li>
                            <li><a href="javascript:void(0);" title="">our products  <i class="fas fa-chevron-down hidden-sm hidden-xs" aria-heddin="true"></i></a><i class="fas fa-chevron-down hidden-md hidden-lg" aria-heddin="true"></i>
                            <ul class="sub-menu">
                                <li id="menu-item-1"><a href="" title="">Accessories</a></li>
                                <li id="menu-item-2"><a href="" title="">Drinkware</a></li>
                                <li id="menu-item-3"><a href="" title="">Stationeries</a></li>
                                <li id="menu-item-4"><a href="" title="">Promotions</a></li>
                                <li id="menu-item-5"><a href="" title="">Life Style</a></li>
                                <li id="menu-item-6"><a href="" title="">Carrier</a></li>
                                <li id="menu-item-8"><a href="" title="">Office</a></li>
                                <li id="menu-item-9"><a href="" title="">Outdoor</a></li>
                                <li id="menu-item-10"><a href="" title="">Gift Sets</a></li>
                                <li id="menu-item-11"><a href="" title="">Impressive Umbrellas</a></li>
                                <li id="menu-item-11"><a href="" title="">Impressive Umbrellas</a></li>
                                <li id="menu-item-11"><a href="" title="">Impressive Umbrellas</a></li>
                            </ul>
                            </li>
                            <li><a href="javascript:void(0);" title="">testimonial</a></li>
                            <li><a href="contacts.php" title="">contact us</a></li>
                            <div class="clearfix"></div>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-7">
                    <div class="search">
                        <form>
                            <input class="inpt" type="text" placeholder="search" name="search">
                            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="us-about">
        <div class="container">
            <div class="breadcrumb projects">
                <a href="javascript:void(0);" title="">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <i class="fas fa-angle-right right" aria-hidden="true"></i>
                    Enquiry
                    </a>
            </div>
        </div>
    </div>
    <div class="container products-container animated bounceInRight delay-5s">
        <div class="text-center table-center">
            <form>
                <table class="table-enquiry table table-bordered">
                    <thead>
                        <th></th>
                        <th class="name-product">product</th>
                        <th class="center-tranform">Quantity</th>
                        <th class="center-tranform">Unit Price</th>
                        <th class="center-tranform">Total</th>
                    </thead>
                    <tbody>
                        <?php
                            $total = 0;
                            if(empty($_SESSION['enquiry'])){
                                echo '<script>';
                                echo 'alert("bạn chưa có sản phẩm nào");';
                                echo '</script>';
                        ?>
                            Your cart is currently empty.<br>
                            <div class="inpt-view-all" style="margin-bottom: 20px; margin-top: 20px;">
                                <a href="mini.php" class="view mobie-btn" title="" style="text-decoration: none;">
                                 Return to Shop
                                </a>
                            </div>

                        <?php
                                die;
                            }
                            foreach ($_SESSION['enquiry'] as $product) {
                                 $id = $product['id'];
                                 $sql = "SELECT *FROM products WHERE id= '$id'";
                                 $result = mysqli_query($con, $sql);
                                 if( mysqli_num_rows($result) > 0 ){
                                    $result = mysqli_fetch_assoc($result);
                                    $priceInt = (int)str_replace("$","" ,$result['price']);
                        ?>
                        <tr>
                            <td class="remove"><a href="delete-enquiry.php?id_product=<?php echo $result['id']?>" class="remove-x" title="">x</a></td>
                            <td class="name-product">
                                <a href="javascript:void(0);" title="">
                                    <img src="<?php echo $result['avata']; ?>" alt="" style="height: 150px;">
                                </a>
                                <a href="javascript:void(0);" title=""><?php echo $result['name_slug']; ?></a>
                            </td>
                            <td class="remove number-with-small">
                                <input type="number" value="<?php echo $product['QUANTITY']; ?>" name="">
                            </td>
                            <td class="remove money with-long">$<?php echo $result['price']; ?></td>
                            <td class="remove money">$<?php echo ((int) $product['QUANTITY'])*$priceInt; ?>
                            </td>
                        </tr>
                        <?php
                                $total = $total + ((int) $product['QUANTITY'])*$priceInt;
                             }
                        }   
                        ?>
                        <div class="total-right">
                            <tr>
                                <td class="border-none-display"></td>
                                <td class="border-none-display"></td>
                                <td class="border-none-display"></td>
                                <td class="bold-er">Sub-total:</td>
                                <td class="organe">$<?php echo $total; ?></td>
                            </tr>
                            <tr>
                                <td class="border-none-display"></td>
                                <td class="border-none-display"></td>
                                <td class="border-none-display"></td>
                                <td class="bold-er">Total:</td>
                                <td class="organe">$<?php echo $total; ?></td>
                            </tr>
                        </div>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="product-enquiry animated bounceInRight delay-5s">
        <div class="container">
            <div class="row">
                <div class="col-md-2 hidden-sm hidden-xs">
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <form action="email.php" method="post" accept-charset="utf-8">
                        <div class="send-enquiry">
                            <div class="h3-product-bold">
                                <h3>Product enquiry</h3>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p class="border-none-bottom">
                                        <input class="inpt-pla" type="text" name="name" value="" placeholder="First name: " required>
                                    </p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p class="border-none-bottom">
                                        <input class="inpt-pla" type="text" name="lastname" value="" placeholder="Last name:">
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p class="border-none-bottom">
                                        <input class="inpt-pla" type="text" name="phone" value="" placeholder="Phone: " required>
                                    </p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p class="border-none-bottom">
                                        <input class="inpt-pla" type="email" name="email" value="" placeholder="Email:" required>
                                    </p>
                                </div>
                            </div>
                            <p class="bottom-long">
                                <textarea class="inpt-pla" name="order_comment" name="message" value="message" placeholder="Request Detail"></textarea> 
                            </p>
                       <!--      <div class="recapcha" style="margin: auto;">
                                <form action="" method="" accept-charset="utf-8">
                                    <div class="g-recaptcha" data-sitekey="6Lcq1nMUAAAAACG2s0wVcF28EX3mpthZ-pOVFUUH"></div><br>
                                </form>
                            </div> -->
                            <div class="center-alia">
                                <input type="submit" class="send-submit" name="submit" value="Send enquiry">
                            </div>
                        </div>
                    </form>    
                </div>
                <div class="col-md-2 hidden-sm hidden-xs">
                </div>
            </div>
        </div>
    </div>
<!--     </div> -->
    <?php include('footer.php'); ?>
    
</body>

</html>