<footer id="footer">
    <div class="footer-1 animated bounceInRight delay-5s">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 footer-logo col-sm-2 col-xs-12">
                    <a href="javascript:void(0);" title="">
                        <img class="img-homepage" src="images/0-Homepage2.png" alt="">
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <div class="border-h5">
                        <h5>impressgift</h5>
                            <!--  <img src="images/footer-redline.png" alt=""> -->
                    </div>
                    <div class="adress  margin-img">
                        <p>
                            <img src="images/address.png" alt=""> 96C Jalan Senang Singapore 418483.
                        </p>
                    </div>
                    <div class="call-contet margin-img">
                        <p>
                            <img src="images/call-contect.png" alt=""> +65 6876 0079
                        </p>
                    </div>
                    <div class="mail margin-img">
                        <p>
                            <img src="images/gmail-icon.png" alt=""> sales@impressgift.com.sg
                        </p>
                    </div>
                    <div class="hour-open margin-img">
                        <img src="images/time-icon.png" alt="">
                        <div class="open">
                            <h5>
                                Hour Opening: 
                            </h5>
                            <p>
                                Mon-Sun: 8:00am - 10:00pm
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 hidden-sm col-xs-12">
                    <div class="quick-link">
                        <div class="link">
                            <div class="clearfix">
                            </div>
                            <h5>
                                Quick Link
                            </h5>
                        </div>
                        <ul class="menu-footer">
                            <li><a href="index.php" title="">Home</a></li>
                            <li><a href="About_us.php" title="">About Us</a></li>
                            <li><a href="project2.php" title="">projects</a></li>
                            <li><a href="our_products.php" title="">Our products</a></li>
                            <li><a href="contacts.php" title="">contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <div class="sign-for">
                        <div class="sign">
                            <h5>
                                signup for our newsletter
                            </h5>
                        </div>
                            <p>
                                Receive our latest updates about our products and promotions
                            </p>
                            <p>
                                <form action="subscribe.php" method="post" accept-charset="utf-8">
                                    <span class="email-subscribe">
                                    <input type="email" class="your-email" placeholder="Enter your email adress" name="email">
                                    <input type="submit" class="submit-subscribe" value="Subscribe" name="submit">
                                    </span>
                                  
                                </form>
                            </p>
                    </div>
                        <div class=" sign-for stay-contact">
                            <div class="sign stay">
                                <h5>
                                    stay connected
                                </h5>
                            </div>
                            <div class="contact-icon">
                                <ul class="stay-icon">
                                    <li><a href="https://www.facebook.com/profile.php?id=100012746058967" title="">
                                        <img src="images/icon-facebook.png" alt="">
                                         </a>
                                    </li>
                                    <li><a href="javascript:void(0);" title="">
                                        <img src="images/icon-gmail.png" alt="">
                                        </a>
                                    </li>
                                    <li><a href="https://www.instagram.com/hapham2591/" title="">
                                        <img src="images/icon-insta.png" alt="">
                                        </a>
                                    </li>
                                    <li><a href="javascript:void(0);" title="">
                                        <img src="images/icon-youtube.png" alt="">
                                        </a>
                                    </li>
                                    <div class="clearfix"></div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="footer-2">
        <div class="container">
            <div class="row">
                <div class="copyright">
                    <p>
                        Copyright © 2018
                        <a href="index.php" title="">Impressgift</a> . All Right Reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="asset/js/bootstrap.min.js"></script>
<script src="asset/js/app.js"></script>
       