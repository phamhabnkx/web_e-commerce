<?php  
    include('config1.php');
    session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>E-commerce project</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="asset/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/asset.css">
    
</head>

<body>
    <?php include('header.php'); ?>
    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-5">
                    <div class="menu-1">
                        <a href="javascript:void(0);" class="menu-bars"><i class="fas fa-bars"></i></a>
                        <ul class="menu-2">
                            <li><a href="index.php" title="">Home</a></li>
                            <li><a href="About_us.php" title="">About us</a></li>
                            <li><a href="our_products.php" title="">projects</a></li>
                            <li><a href="javascript:void(0);" title="">our products  <i class="fas fa-chevron-down hidden-xs hidden-sm" aria-heddin="true"></i></a><i class="fas fa-chevron-down hidden-md hidden-lg" aria-heddin="true"></i>
                                <ul class="sub-menu">
                                    <li id="menu-item-1"><a href="" title="">Accessories</a></li>
                                    <li id="menu-item-2"><a href="" title="">Drinkware</a></li>
                                    <li id="menu-item-3"><a href="" title="">Stationeries</a></li>
                                    <li id="menu-item-4"><a href="" title="">Promotions</a></li>
                                    <li id="menu-item-5"><a href="" title="">Life Style</a></li>
                                    <li id="menu-item-6"><a href="" title="">Carrier</a></li>
                                    <li id="menu-item-8"><a href="" title="">Office</a></li>
                                    <li id="menu-item-9"><a href="" title="">Outdoor</a></li>
                                    <li id="menu-item-10"><a href="" title="">Gift Sets</a></li>
                                    <li id="menu-item-11"><a href="" title="">Impressive Umbrellas</a></li>
                                    <li id="menu-item-11"><a href="" title="">Impressive Umbrellas</a></li>
                                    <li id="menu-item-11"><a href="" title="">Impressive Umbrellas</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:void(0);" title="">testimonial</a></li>
                            <li class="active"><a href="contacts.php" title="">contact us</a></li>
                            <div class="clearfix"></div>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-7">
                    <div class="search">
                        <form>
                            <input class="inpt" type="text" placeholder="search" name="search">
                            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="us-about">
        <div class="container">
            <div class="breadcrumb projects">
                <a href="javascript:void(0);" title="">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <i class="fas fa-angle-right right" aria-hidden="true"></i>
                    Contact Us
                    </a>
            </div>
        </div>
    </div>
    <div class="contacts-web animated bounceInRight delay-3s">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="border-h5 get-in-touch">
                        <h5>
                                Get in touch with us
                            </h5>
                        <!--  <img src="images/footer-redline.png" alt=""> -->
                    </div>
                    <div class="adress contacts">
                        <p>
                            <i class="fas fa-map-marker-alt adress-call-mail" aria-hidden="true"></i>96C Jalan Senang Singapore 418483.
                        </p>
                    </div>
                    <div class="call-contet contacts">
                        <p>
                            <i class="fas fa-phone-volume adress-call-mail" aria-hidden="true"></i> +65 6876 0079
                        </p>
                    </div>
                    <div class="mail contacts">
                        <p>
                            <i class="fas fa-envelope adress-call-mail" aria-hidden="true"></i>sales@impressgift.com.sg
                        </p>
                    </div>
                    <form action="email.php" method="post" accept-charset="utf-8">
                        <div class="padding-right-60px">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p class="border-none-bottom">
                                        <input type="text" name="name" value="" placeholder="First name: " required="">
                                    </p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p class="border-none-bottom">
                                        <input type="text" name="lastname" value="" placeholder="Last name:">
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p class="border-none-bottom">
                                        <input type="text" name="phone" value="" placeholder="Phone: " required="">
                                    </p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p class="border-none-bottom">
                                        <input type="text" name="email" value="" placeholder="Email:" required="">
                                    </p>
                                </div>
                            </div>
                            <p class="bottom-long">
                                <textarea name="message" placeholder="Request Detail"></textarea> 
                            </p>
                        </div>
                     
                        <div class="input-contact-submit">
                            <input type="submit" class="value-submit" name="submit" value="submit request">
                        </div>
                   
                    </form>
                    
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="maps">
                        <iframe class="iframe-info" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.748327390348!2d103.9145987142957!3d1.3269703990321815!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da3d552436d147%3A0x5c44e2e5a28d211!2s96C+Jalan+Senang%2C+Xinh-ga-po+418483!5e0!3m2!1svi!2s!4v1533114673343" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
    </script> 
</body>

</html>