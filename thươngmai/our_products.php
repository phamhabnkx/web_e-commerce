<?php
  include('config1.php');
  session_start();  
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>E-commerce project</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="asset/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/asset.css">
    
</head>

<body>
    <?php include('header.php'); ?>
    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-5">
                    <div class="menu-1">
                        <a href="javascript:void(0);" class="menu-bars"><i class="fas fa-bars"></i></a>
                        <ul class="menu-2">
                            <li><a href="index.php" title="">Home</a></li>
                            <li><a href="About_us.php" title="">About us</a></li>
                            <li><a href="project2.php" title="">projects</a></li>
                            <li class="active"><a href="our_products.php" title="">colection</a></li>
                            <li><a href="#" title="">testimonial</a></li>
                            <li><a href="contacts.php" title="">contact us</a></li>
                            <div class="clearfix"></div>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-7">
                    <div class="search">
                        <form>
                            <input class="inpt" type="text" placeholder="search" name="search">
                            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="us-about">
        <div class="container">
            <div class="breadcrumb">
                <a href="javascript:void(0);" title="">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <i class="fas fa-angle-right right" aria-hidden="true"></i>
                    Projects
                    </a>
            </div>
        </div>
    </div>
    <div class="pruducts animated bounceInRight delay-5s">
        <div class="container">
            <div class="row">
                <?php include('product_img.php'); ?>
            </div>
        </div>
    </div>
    <nav class="clearfix text-center">
        <ul class="pagination">
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">>></a></li>
        </ul>
    </nav>
   
    <?php include('footer.php'); ?>
</body>

</html>