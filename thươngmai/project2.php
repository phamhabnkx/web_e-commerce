<?php 
    include('config1.php'); 
    session_start(); 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>E-commerce project</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" href="asset/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="asset/css/asset.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <div class="menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8 col-xs-5">
                        <div class="menu-1">
                            <a href="javascript:void(0);" class="menu-bars"><i class="fas fa-bars"></i></a>
                            <ul class="menu-2 menu3">
                                 <li class="right-delete">
                                    <button class="trigger-verlay mobile-trigger-overlay hidden-md hidden-lg">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                </li>
                                <li><a href="mini.php" title="">Home</a></li>
                                <li><a href="About_us.php" title="">About us</a></li>
                                <li><a href="project2.php" title="">projects</a></li>
                                <li><a href="our_products.php" title="">colection</a></li>
                                <li><a href="javascript:void(0);" title="">testimonial</a></li>
                                <li><a href="contacts.php" title="">contact us</a></li>
                                <div class="clearfix"></div>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-7">
                        <div class="search">
                            <form>
                                <input class="inpt" type="text" placeholder="search" name="search">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="us-about">
            <div class="container">
                <div class="breadcrumb projects">
                    <a href="javascript:void(0);" title="">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <i class="fas fa-angle-right right" aria-hidden="true"></i>
                    Our Products  
                    <i class="fas fa-angle-right right" aria-hidden="true"></i>
                    Lorem ipsum dolor
                    </a>
                </div>
            </div>
        </div>
        <div class="bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="tool-bar">
                            <?php include('sidebar.php'); ?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-12 col-xs-12 animated bounceInRight delay-5s">
                        <div class="row">
                            <?php
                                if(empty( $_GET['id']) ){
                                   echo '<script type="text/javascript">';
                                   echo 'alert("Lỗi !")';
                                   header('location:"/thươngmai/project2";');
                                   echo '</script>';
                            ?>
                                <div class="inpt-view-all" style="margin-bottom: 20px; margin-top: 20px;">
                                    <a href="mini.php" class="view mobie-btn" title="" style="text-decoration: none;">
                                     Return to Shop
                                    </a>
                                </div>

                            <?php  
                                   die;
                                  
                                } 
                                $id = $_GET['id'];
                                $sql3 = " SELECT *FROM products WHERE id =$id";
                                $result3 = mysqli_query($con,$sql3);
                                if(mysqli_num_rows($result3) <= 0 ){      
                                    die;
                                } 
                                $row = mysqli_fetch_assoc($result3);
                                if(!$result3){
                                   echo '<script>';
                                   echo 'alert("Dữ liệu không tồn tại")';
                                   echo 'window.location.href="";';
                                   echo '</script>';
                                 }
                                ?>
                            <div class="col-md-6 col-sm-12 col-xs-12 center-img-products">
                                <img src="<?php echo $row['avata']; ?>" alt="" style="width: 376px;">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="profile">
                                    <h2><?php echo $row['name_slug']; ?></h2>
                                    <p><?php echo $row['description']; ?> </p>
                                    <div class="dola">
                                        <p> $<?php echo $row['price']; ?>.00</p>
                                    </div>
                                    <div class="btn-equiry-now">
                                        <form action="add_enquiry.php" method="POST" accept-charset="utf-8">
                                            <input type="hidden" name="product_id" value="<?php echo $row['id']?>">
                                            <a>   
                                                <button type="submit">Enquiry Now</button>
                                            </a>   
                                        </form>
                                    </div>
                                    <div class="btn-face-intagr-titew">
                                        <a class="btn-face-ing facebook1" href="https://www.facebook.com/profile.php?id=100012746058967" title="">
                                        <i class="fab fa-facebook" aria-hidden="true"></i>
                                        Facebook
                                        </a>
                                        <a class="btn-face-ing twitter" href="https://twitter.com/PhmThTh38279842" title="">
                                        <i class="fab fa-twitter" aria-hidden="true"></i>
                                        Twitter
                                        </a>
                                        <a class="btn-face-ing google" href="https://plus.google.com/u/0/" title="">
                                        <i class="fab fa-google-plus" aria-hidden="true"></i>
                                        Google+
                                        </a>
                                        <a class="btn-face-ing pinterest" href="https://www.pinterest.com/" title="">
                                        <i class="fab fa-pinterest" aria-hidden="true"></i>
                                        Pinterest
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="description">
                            <h2>
                                Description
                            </h2>
                            <div class="products-content-border">
                                <?php echo $row['content']; ?>
                            </div>
                        </div>
                        <h2 class="new-products">
                            related products
                        </h2>
                        <div class="columns-4">
                            <ul class="products-columns-4">
                                <?php include('image.php'); ?>
                                <div class="clearfix"></div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('footer.php'); ?>
    </body>
</html>