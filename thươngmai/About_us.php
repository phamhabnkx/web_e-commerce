<?php
    include('config1.php');
    session_start();  
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>E-commerce project</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="asset/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/asset.css">
    
</head>

<body>
    <header id="header" class="header animated bounceInRight delay-3s">
        <div class="header-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="login">
                            <p>
                                Login | Register
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="equi">
                            <img src="images/equi-icon.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="heade-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-3 col-xs-12">
                        <div class="header-item">
                            <a href="About_us.php" title=""><img class="header-item1" src="images/0-Homepage2.png" alt=""></a>
                            
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-9 hidden-xs">
                        <div class="all abouts-us-info">
                            <div class="ship free-info-header">
                                <div class="item">
                                    <img class="header-item2" src="images/oto.png" alt="">
                                </div>
                                <div class="free">
                                    <h4> free shipping</h4>
                                    <p>Free shipping on all order</p>
                                </div>
                            </div>
                            <div class="ship call-ship">
                                <div class="item">
                                    <img class="header-item3" src="images/header-icon1.png" alt="">
                                    <div class="clearfix">
                                </div>
                                </div>
                                
                                <div class="free suppost">
                                    <h4>Suppost 24/7</h4>
                                    <p>We support online 24 hours a day</p>
                                </div>
                                <div class="clearfix">

                                </div>
                            </div>
                            <div class="ship hotline">
                                <div class="item">
                                    <img class="header-item3" src="images/call.png" alt="">
                                </div>
                                <div class="free">
                                    <h4> HOTLINE</h4>
                                    <p>+65 6876 0079</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8 col-xs-5">
                    <div class="menu-1">
                        <a href="javascript:void(0);" class="menu-bars"><i class="fas fa-bars"></i></a>
                        <ul class="menu-2 menu3">
                            <li class="right-delete">
                                <button class="trigger-verlay mobile-trigger-overlay hidden-md hidden-lg">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </li>
                            <li><a href="index.php" title="">Home</a></li>
                            <li class="active"><a href="About_us.php" title="">About us</a></li>
                            <li><a href="our_products.php" title="">projects</a></li>
                            <li><a href="javascript:void(0);" title="">colection</a></li>
                            <li><a href="javascript:void(0);" title="">testimonial</a></li>
                            <li><a href="contacts.php" title="">contact us</a></li>
                            <div class="clearfix"></div>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-7">
                    <div class="search">
                        <form>
                            <input class="inpt" type="text" placeholder="search" name="search">
                            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="us-about">
        <div class="container">
            <div class="breadcrumb">
                <a href="javascript:void(0);" title="">
    				<i class="fa fa-home" aria-hidden="true"></i>
    			    <i class="fas fa-angle-right right" aria-hidden="true"></i>
    			    About Us
    			    </a>
            </div>
        </div>
    </div>
    <div class="about animated bounceInRight delay-3s">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="impress-gitf">
                        <p>
                            Impress Gift specialise in corporate gift in Singapore. We pride ourselves in giving fast and reliable Quality services to all our customer. We provide the Best Gifting to you. We manage import and export on gifts to Europe and Asia. We have our very own sourcing Department in our factory which gives us the advantage to give you Direct Factory Pricing.
                        </p>
                    </div>
                    <div class="strong-why">
                        <strong>
    						Why Impress Gift
    					</strong>
                    </div>
                    <div class="check-circle">
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i>Because we believed that</p>
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i>A GIFT that is handed out, will provide an impression to the customer.
                        </p>
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i>A GIFT that is handed out, represent your company</p>
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i>A GIFT that is handed out, creates a first impression BONDing between</p>
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i>We believe in QUALITY Assurance
                        </p>
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i>We believe in High Perceive Value and Practicality of Gifting</p>
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i>We believe in Advertising and Promotional branding with Gifting</p>
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i>We believe in Advertising and Promotional branding with Gifting</p>
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i>We love the Season OF MPRESSIVE GIFTING
                        </p>
                        <p><i class="fas fa-check-circle check-icon" aria-hidden="true"></i> LASTLY, Providing EXCELLENT CUSTOMER SERVICE is our PRACTISE.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <img class="content-banner animated bounceInRight delay-3s" src="images/content-banner.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
</body>

</html>