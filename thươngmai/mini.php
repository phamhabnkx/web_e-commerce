<?php 
    include('config1.php');
    session_start(); 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>E-commerce project</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" href="dist/css/swiper.min.css">
        <link rel="stylesheet" href="asset/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="asset/css/asset.css">
    </head>
    <body>
       <?php include('header.php'); ?>
       <?php include('menu.php'); ?>
        <div class="bar">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="tool-bar">
                       <?php include('sidebar.php'); ?> 
                    <div class="clearfix"></div>
                    </div>
                    <div class="imgabc">
                        <a href="javascript:void(0);" title="" class="hidden-sm hidden-xs"><img class="img333" src="images/333.jpg" alt=""></a>
                    </div>
                    <div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="swiper-project" style="margin-top: 47px;">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide"><img src="images/banner_eventos.jpg" alt=""></div>
                              <div class="swiper-slide"><img src="images/banner_eventos.jpg" alt=""></div>
                              <div class="swiper-slide"><img src="images/banner_eventos.jpg" alt=""></div>
                            
                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                    <div class="feature-products">
                        <h2 class="feat">
                            feature products
                        </h2>
                        <div class="woocommerce columns-4">
                            <ul class="products-columns-4">
                                <?php include('image.php'); ?>
                                <div class="clearfix"></div>
                            </ul>
                            <div class="inpt-view-all">
                                <a href="javascript:void(0);" class="view" title="">
                                view all
                                <i class="abc fas fa-chevron-right"></i>
                                </a>
                            </div>
                            <div class="panner">
                                <a href="javascript:void(0);" title="">
                                <img src="images/content-banner1.png" alt="">
                                </a>
                            </div>
                            <div class="feature-products">
                                <h2 class="new-products">
                                    new products
                                </h2>
                                <div class="columns-4">
                                    <ul class="products-columns-4 ul-li">
                                    <?php include('image2.php'); ?>
                                    <div class="clearfix"></div>
                                </ul>
                            </div>
                            <div class="inpt-view-all">
                                    <a href="javascript:void(0);" class="view" title="">
                                    view all
                                    <i class="abc fas fa-chevron-right"></i>
                                    </a>
                            </div>
                            </div>
                            <div class="feature-products">
                                <h2 class="new-products">
                                    gift sets & baskets
                                </h2>
                                <div class="columns-4">
                                    <ul class="products-columns-4 ul-li">
                                        <?php include('image2.php'); ?>
                                        <div class="clearfix"></div>
                                    </ul>
                                <div class="inpt-view-all view-last">
                                        <a href="javascript:void(0);" class="view" title="">
                                        view all
                                        <i class="abc fas fa-chevron-right"></i>
                                        </a>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php'); ?>
    <script src="dist/js/swiper.min.js"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        });
    </script>
    </body>
</html>