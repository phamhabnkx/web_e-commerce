<?php 
    include('config1.php'); 
    session_start(); 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>E-commerce project</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" href="asset/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="asset/css/asset.css">
    </head>
    <body>
        <?php include('header.php'); ?>
        <div class="menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8 col-xs-5">
                        <div class="menu-1">
                            <a href="#" class="menu-bars"><i class="fas fa-bars"></i></a>
                            <ul class="menu-2">
                                <li><a href="index.php" title="">Home</a></li>
                                <li><a href="About_us.php" title="">About us</a></li>
                                <li class="active"><a href="our_products.php" title="">projects</a></li>
                                <li><a href="#" title="">our products  <i class="fas fa-chevron-down" aria-heddin="true"></i></a></li>
                                <li><a href="#" title="">testimonial</a></li>
                                <li><a href="contacts.php" title="">contact us</a></li>
                                <div class="clearfix"></div>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-7">
                        <div class="search">
                            <form>
                                <input class="inpt" type="text" placeholder="search" name="search">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="us-about">
        <div class="container">
            <div class="breadcrumb projects">
                <a href="#" title="">
                <i class="fa fa-home" aria-hidden="true"></i>
                <i class="fas fa-angle-right right" aria-hidden="true"></i>
                Our Products  
                <i class="fas fa-angle-right right" aria-hidden="true"></i>
                Lorem ipsum dolor
                </a>
            </div>
        </div>
        <div class="bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12 margin-top-barsize">
                        <div class="tool-bar">
                            <?php include('sidebar.php'); ?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-12 col-xs-12 margin-top-feature">
                        <div class="feature-products animated bounceInRight delay-5s">
                            <h2 class="feat our--products">
                                our products
                            </h2>
                            <div class="columns-4">
                                <ul class="products-columns-4">
                                    <?php 
                                        for($j = 0; $j < 3 ; $j++){
                                            include('image.php');
                                            echo '<div class="clearfix"></div>';
                                        }
                                        
                                        ?>
                                </ul>
                                <div class="inpt-view-all read-more-down read-more-bottom">
                                    <a href="javascript:void(0);" class="view" title="">
                                    Read more
                                    <i class="fas fa-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('footer.php'); ?>
    </body>
</html>